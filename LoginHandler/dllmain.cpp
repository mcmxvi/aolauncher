#include <windows.h>
#include <detours.h>
#include <string>

bool LoggedIn = false;

typedef void* (__cdecl* Client_GetInstanceIfAny_t) ();
Client_GetInstanceIfAny_t Client_GetInstanceIfAny;

typedef std::string* (* Client_GetPlayerName_t) ();
Client_GetPlayerName_t Client_GetPlayerName;

typedef std::string* (*Client_GetPassword_t) ();
Client_GetPassword_t Client_GetPassword;

typedef void (*Client_SetCharID_t) (unsigned int charId);
Client_SetCharID_t Client_SetCharID;

typedef int(__thiscall* Client_ConnectToLH_t) (void* pClient);
Client_ConnectToLH_t Client_ConnectToLH;

typedef int(__thiscall* Client_LoginCharacter_t) (void* pClient);
Client_LoginCharacter_t Client_LoginCharacter;

typedef void(__thiscall* LoginModule_Initialize_t) (void* pLoginModule, bool loginAccount);
LoginModule_Initialize_t LoginModule_Initialize;

typedef void(__thiscall* LoginModule_Show_t) (void* pLoginModule, int state);
LoginModule_Show_t LoginModule_Show;

typedef void*(__thiscall* Utils_Assign_t) (std::string* string, const char* newString);
Utils_Assign_t Utils_Assign;

void __fastcall LoginModule_Initialize_Hook(void* pLoginModule, int edx, bool loginAccount)
{
    LoginModule_Initialize(pLoginModule, loginAccount);

    Utils_Assign(Client_GetPlayerName(), std::getenv("AOUser"));
    Utils_Assign(Client_GetPassword(), std::getenv("AOPass"));
    Client_ConnectToLH(Client_GetInstanceIfAny());
}

void __fastcall LoginModule_Show_Hook(void* pLoginModule, int edx, int state)
{
    if (state == 3)
    {
        Client_SetCharID(atoi(std::getenv("AOCharId")));
        Client_LoginCharacter(Client_GetInstanceIfAny());
        LoggedIn = true;
    }

    if(strcmp(std::getenv("QuietLaunch"), "False") == 0)
        LoginModule_Show(pLoginModule, state);
}

void LoadAddresses()
{
    HMODULE hInterfaces = GetModuleHandle("Interfaces.dll");
    HMODULE hUtils = GetModuleHandle("Utils.dll");

    Client_GetInstanceIfAny = (Client_GetInstanceIfAny_t)GetProcAddress(hInterfaces, "?GetInstanceIfAny@Client_t@@SAPAV1@XZ");
    Client_GetPlayerName = (Client_GetPlayerName_t)GetProcAddress(hInterfaces, "?GetPlayerName@Client_t@@SAABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@XZ");
    Client_GetPassword = (Client_GetPassword_t)GetProcAddress(hInterfaces, "?GetPassword@Client_t@@SAABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@XZ");
    Client_SetCharID = (Client_SetCharID_t)GetProcAddress(hInterfaces, "?SetCharID@Client_t@@SAXI@Z");
    Client_ConnectToLH = (Client_ConnectToLH_t)GetProcAddress(hInterfaces, "?ConnectToLH@Client_t@@QAEHXZ");
    Client_LoginCharacter = (Client_LoginCharacter_t)GetProcAddress(hInterfaces, "?LoginCharacter@Client_t@@QAEXXZ");
    Utils_Assign = (Utils_Assign_t)GetProcAddress(hUtils, "?assign@String@@QAEAAV1@PBD@Z");
}

int FindAndAttach(LPCSTR pszModule, LPCSTR pszFunction, PVOID* ppPointer, PVOID pDetour) 
{
    auto function = DetourFindFunction(pszModule, pszFunction);

    if (function == NULL)
        return ERROR_INVALID_HANDLE;

    *ppPointer = function;
    return DetourAttach(ppPointer, pDetour);
}

void ThreadMain(HMODULE hModule)
{
    LoadAddresses();

    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    FindAndAttach("GUI.dll", "?SlotInitialize@LoginModule_c@@AAEX_N@Z", (void**)&LoginModule_Initialize, LoginModule_Initialize_Hook);
    FindAndAttach("GUI.dll", "?Show@LoginModule_c@@AAEXW4State_e@1@@Z", (void**)&LoginModule_Show, LoginModule_Show_Hook);
    DetourTransactionCommit();

    while (!LoggedIn)
        Sleep(100);

    DetourTransactionBegin();
    DetourDetach((PVOID*)&LoginModule_Initialize, (PVOID)LoginModule_Initialize_Hook);
    DetourDetach((PVOID*)&LoginModule_Show, (PVOID)LoginModule_Show_Hook);
    DetourTransactionCommit();

    FreeLibraryAndExitThread(hModule, NULL);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        DisableThreadLibraryCalls(hModule);
        CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ThreadMain, hModule, NULL, NULL);
        break;
    }
    return TRUE;
}