# help.py

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QTextEdit, QVBoxLayout, QWidget

class HelpTab(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()

        self.text_edit = QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit.setTextInteractionFlags(Qt.TextSelectableByMouse)

        layout.addWidget(self.text_edit)
        self.setLayout(layout)

        help_text = """
        AOLauncher Help

        1. Using AOLauncher:
        To use the AOLauncher, you must first define the path of your game through the settings tab. Once you've done that, you can add characters and profiles to be launched. A profile can contain multiple characters and will launch all of them simultaneously.

        2. Adding Characters and Profiles:
        To add a character or profile, navigate to the corresponding tab and click on the 'Add' button. Fill out the necessary fields in the dialog that appears.

        3. Launching Characters and Profiles:
        To launch a character or profile, simply select it from the list and click 'Launch', or double click the highlighted character or profile. You can launch multiple characters in the order they are selected from the character tab.

        4. Troubleshooting:
        - If you receive an error stating that 'AOQuickLauncher.dll' is not found, ensure that you have downloaded the file from the provided GitLab link and placed it in the same directory as AOLauncher.
        - If the 'Launch' button is not working, ensure that you've correctly defined the game path in the settings. If the problem persists, try restarting the application or your computer.
        - If you are having issues with a profile, try launching the characters individually to isolate the issue.
        """
        self.text_edit.setText(help_text)
