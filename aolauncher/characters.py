# characters.py

import os
import json
import time
import subprocess
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QAbstractItemView, QCheckBox, QDialog, QFileDialog, QHBoxLayout, QHeaderView, QLabel, QLineEdit, QPushButton, QMessageBox, QPushButton, QTableWidget, QTableWidgetItem, QVBoxLayout, QWidget
from PySide6.QtGui import QKeySequence, QShortcut
from openpyxl import load_workbook
from utils import create_context_menu, get_character_bio, save_character_data


class CharactersTab(QWidget):
    def __init__(self, profilesTab, parent=None):
        super().__init__(parent)

        self.profilesTab = profilesTab

        # Create a horizontal layout for the buttons
        self.buttonsLayout = QHBoxLayout()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.table = QTableWidget()
        self.table.setColumnCount(5)
        self.table.setSortingEnabled(True)
        self.table.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.table.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
        self.table.setContextMenuPolicy(Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.on_table_context_menu_requested)
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.table.setHorizontalHeaderLabels(['Bio', 'Character', 'Account Name', 'Password', 'CharID'])

        
        self.table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        self.table.setColumnWidth(0, 100)

        for i in range (1, self.table.columnCount()):
            self.table.horizontalHeader().setSectionResizeMode(i, QHeaderView.Stretch)
        
        self.table.setColumnHidden(3, True)
        self.layout.addWidget(self.table)

        self.launchCharactersButton = QPushButton("Launch character(s)")
        self.addCharacterButton = QPushButton("Add character")
        self.editCharacterButton = QPushButton("Edit character")
        self.deleteCharacterButton = QPushButton("Delete character(s)")
        self.importCharactersButton = QPushButton("Import characters")
        self.showPasswordsCheckbox = QCheckBox("Show passwords")

        # Add buttons to the horizontal layout
        self.buttonsLayout.addWidget(self.launchCharactersButton)
        self.buttonsLayout.addWidget(self.addCharacterButton)
        self.buttonsLayout.addWidget(self.editCharacterButton)
        self.buttonsLayout.addWidget(self.deleteCharacterButton)
        self.buttonsLayout.addWidget(self.importCharactersButton)
        self.buttonsLayout.addWidget(self.showPasswordsCheckbox)

        # Connect buttons to functions
        self.launchCharactersButton.clicked.connect(self.on_launch_characters_clicked)
        self.addCharacterButton.clicked.connect(self.on_add_character_clicked)
        self.editCharacterButton.clicked.connect(self.on_edit_character_clicked)
        self.deleteCharacterButton.clicked.connect(self.on_delete_character_clicked)
        self.importCharactersButton.clicked.connect(self.on_import_characters_clicked)

        self.delete_shortcut = QShortcut(QKeySequence(Qt.Key_Delete), self.table)
        self.delete_shortcut.activated.connect(self.on_delete_character_clicked)

        # Add the buttons layout to the main layout
        self.layout.addLayout(self.buttonsLayout)

        # Connect checkbox state change signal to slot
        self.showPasswordsCheckbox.stateChanged.connect(self.on_showPasswordsCheckbox_stateChanged)

        self.load_data()

    def on_table_context_menu_requested(self, point):
        options = [
            ("Launch characters(s)", self.on_launch_characters_clicked),
            ("Edit character", self.on_edit_character_clicked),
            ("Delete character", self.on_delete_character_clicked),
        ]
        context_menu = create_context_menu(self, options)
        context_menu.exec_(self.table.mapToGlobal(point))

    def load_data(self):
        try:
            with open('characters.json', 'r') as f:
                data = json.load(f)
        except FileNotFoundError:
            data = []
            # Create an empty characters.json
            with open('characters.json', 'w') as f:
                json.dump(data, f)

        for character_data in data:
            if time.time() - character_data.get('last_updated', 0) > 48 * 60 * 60:  # 48 hours
                character_data['bio'] = get_character_bio(character_data['character_name'])
                character_data['last_updated'] = time.time()

            self.add_character_to_table(character_data)

        # Save updated data
        with open('characters.json', 'w') as f:
            json.dump(data, f)


    def on_showPasswordsCheckbox_stateChanged(self, state):
        # If the checkbox is checked, show the password column; otherwise, hide it
        self.table.setColumnHidden(3, not state)

    def on_launch_characters_clicked(self):
        selected_rows = self.table.selectionModel().selectedRows()

        if not selected_rows:
            return

        bat_file_path = os.path.join(os.getcwd(), "Login.bat")
        
        # Load game path
        with open('settings.json', 'r') as f:
            settings = json.load(f)
        game_path = settings.get('game_path')

        # Set the environment variable
        os.environ['AOPath'] = game_path
        print("Set AOPath environment variable to:", os.environ['AOPath'])

        # Fetch character data from table and add to bat file
        account_names = []
        duplicates = []
        with open(bat_file_path, "w") as bat_file:
            bat_file.write("@ECHO OFF\n")
            bat_file.write(f'set "AOPath={game_path}"\n')
            bat_file.write('set "DLLPath=%~dp0AOQuickLauncher.dll"\n')
            for row_item in selected_rows:
                row = row_item.row()
                account_name = self.table.item(row, 2).text()
                character_name = self.table.item(row, 1).text()
                if account_name in account_names:
                    duplicates.append(character_name)
                else:
                    account_names.append(account_name)
                    password = self.table.item(row, 3).text()
                    char_id = self.table.item(row, 4).text()
                    bat_file.write(f"dotnet %DLLPath% {account_name} {password} {char_id}\n")
                    bat_file.write("timeout /t 1 /nobreak\n")

        if duplicates:
            QMessageBox.warning(
                self,
                "Duplicate Account Names",
                f"One or more characters in the selection are sharing an account with {', '.join(duplicates)}",
            )
            return

        # Run the bat file
        subprocess.run([bat_file_path], shell=True)

    def is_valid_character(self, character_data, row_being_edited=None):
        # Check if nickname and char_id are not empty
        for i in range(self.table.rowCount()):
            # Skip the row being edited
            if (self.table.item(i, 1).text() == character_data['character_name'] and
                i != row_being_edited):
                return False

            if (self.table.item(i, 4).text() == character_data['char_id'] and
                i != row_being_edited):
                return False

        return True

        # Load existing data from character.json
        with open ('characters.json', 'r') as f:
            data = json.load(f)

        # Check if the nickname and char_id are unique
        for existing_character_data in data:
            if existing_character_data['character_name'] == character_data['character_name'] or existing_character_data['char_id'] == character_data['char_id']:
                return False

        return True

    def show_warning_dialog(self, message):
        QMessageBox.warning(self, "Warning", message)
     
    def on_add_character_clicked(self):
        self.add_character_dialog = AddCharacterDialog(self)
        while True:
            result = self.add_character_dialog.exec_()
            if result == QDialog.Accepted:
                # Fetch new character data from dialog
                new_character_data = self.add_character_dialog.get_data()

                # Check if character data is valid
                if not self.is_valid_character(new_character_data):
                    print(f"Character data matches existing character")
                    self.show_warning_dialog("New character matches existing character")
                else:
                    # Append character data to table
                    self.add_character_to_table(new_character_data)

                    # Append character data to characters.json
                    self.add_character_to_json(new_character_data)
                    break
            else:
                break

    def add_character_to_table(self, character_data):
        row = self.table.rowCount()  # get current number of rows
        self.table.insertRow(row)  # insert a new row at the end

        # set new row's data
        self.table.setItem(row, 0, QTableWidgetItem(character_data['bio']))  # Bio
        self.table.setItem(row, 1, QTableWidgetItem(character_data['character_name']))  # Character name
        self.table.setItem(row, 2, QTableWidgetItem(character_data['account_name']))  # Account name
        self.table.setItem(row, 3, QTableWidgetItem(character_data['password']))  # Password
        self.table.setItem(row, 4, QTableWidgetItem(character_data['char_id']))  # Char ID

        print(f"Adding character to table: {character_data}")

    def add_character_to_json(self, character_data):
        # Load existing data from characters.json
        try:
            with open('characters.json', 'r') as f:
                data = json.load(f)
        except FileNotFoundError:
            data = []

        # Append new character data
        data.append(character_data)

        # Write updated data back to characters.json
        with open('characters.json', 'w') as f:
            json.dump(data, f)

    def on_edit_character_clicked(self):
        row = self.table.currentRow()
        if row == -1:
            return

        #Fetch character data from table
        current_character_data = {
            'character_name': self.table.item(row, 1).text(),
            'account_name': self.table.item(row, 2).text(),
            'password': self.table.item(row, 3).text(),
            'char_id': self.table.item(row, 4).text(),
        }

        # Open an "Edit Character" dialog pre-filled with the selected character's current data
        self.on_edit_character_dialog = AddCharacterDialog(self, current_character_data)
        while True:
            result = self.on_edit_character_dialog.exec_()
            if result == QDialog.Accepted:
                # Fetch updated character data from dialog
                updated_character_data = self.on_edit_character_dialog.get_data()

                # Check if character data is valid
                if not self.is_valid_character(updated_character_data, row):
                    print(f"Character data matches existing character")
                    self.show_warning_dialog("New character matches existing character")
                else:
                    # Update character data in table
                    self.update_character_in_table(row, updated_character_data)

                    # Update character data in characters.json
                    self.update_character_in_json(row, updated_character_data)
                    print(f"Successfully updated {updated_character_data}")
                    break
            else:
                break

    def update_character_in_table(self, row, character_data):
        # Update row's data
        self.table.setItem(row, 1, QTableWidgetItem(character_data['character_name']))
        self.table.setItem(row, 2, QTableWidgetItem(character_data['account_name']))
        self.table.setItem(row, 3, QTableWidgetItem(character_data['password']))
        self.table.setItem(row, 4, QTableWidgetItem(str(character_data['char_id'])))

    def update_character_in_json(self, row, character_data):
        # Load existing data from characters.json
        with open ('characters.json', 'r') as f:
            data = json.load(f)

        # Update character data
        data[row] = character_data

        # Write update data back to characters.json
        with open ('characters.json', 'w') as f:
            json.dump(data, f)

    def on_delete_character_clicked(self):
        selected_rows = self.table.selectionModel().selectedRows()

        if not selected_rows:
            return

        # Sort the rows in reverse order so that removing multiple rows doesn't affect the subsequent removals
        sorted_rows = sorted(selected_rows, key=lambda row: row.row(), reverse=True)

        for row_item in sorted_rows:
            row = row_item.row()
            self.table.removeRow(row) # Remove row from table
            self.delete_character_from_json(row) # Update characters.json

    def delete_character_from_json(self, row):
        # Load existing data from characters.json
        with open('characters.json', 'r') as f:
            data = json.load(f)
        
        # Get the name of the character to delete
        character_to_delete = data[row]['character_name']

        # Remove the respective entry
        data.pop(row)

        # Write the updated data back to characters.json
        with open('characters.json', 'w') as f:
            json.dump(data, f)

        # Load existing data from profiles.json
        with open('profiles.json', 'r') as f:
            profile_data = json.load(f)

        # Iterate through each profile and remove the character if it's in the profile
        for profile in profile_data:
            if character_to_delete in profile['characters']:
                profile['characters'].remove(character_to_delete)

        # Write the updated data back to profiles.json
        with open('profiles.json', 'w') as f:
            json.dump(profile_data, f)

        # After deleting character from profiles, refresh the profiles tab
        self.profilesTab.refresh_profiles()

    def on_import_characters_clicked(self):
        info_dialog = QMessageBox()
        info_dialog.setIcon(QMessageBox.Information)
        info_dialog.setWindowTitle("Important!")
        info_dialog.setText("The excel file should have the following headers:\nColumn A: Character\nColumn B: Account name\nColumn C: Password\nColumn D: Char ID")
        info_dialog.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        response = info_dialog.exec()

        if response == QMessageBox.Ok:
            file_dialog = QFileDialog()
            file_path, _ = file_dialog.getOpenFileName(self, "Select an Excel file", "", "Excel Files (*.xlsx)")

            if file_path:
                self.import_characters_from_excel(file_path)

    def import_characters_from_excel(self, file_path):
        # Load the workbook
        wb = load_workbook(filename=file_path)
        ws = wb.active # get the active worksheet

        if ws['A1'].value.upper() != 'CHARACTER' or ws['B1'].value.upper() != 'ACCOUNT NAME' or ws['C1'].value.upper() != 'PASSWORD' or ws['D1'].value.upper() != 'CHAR ID':
            ws.insert_rows(1)
            ws['A1'].value = 'CHARACTER'
            ws['B1'].value = 'ACCOUNT NAME'
            ws['C1'].value = 'PASSWORD'
            ws['D1'].value = 'CHAR ID'
            wb.save(file_path)

        duplicate_characters = []
        new_characters = []

        # Load existing characters from JSON
        with open('characters.json', 'r') as f:
            existing_characters = json.load(f)
        existing_char_ids = {char['char_id'] for char in existing_characters}
        
        for row in ws.iter_rows(min_row=2, values_only=True): # skip the header row
            character_data = {
                'character_name': row[0],
                'account_name': row[1],
                'password': row[2],
                'char_id': str(row[3]),
            }

            print(f"Importing character from Excel: {character_data}")

            if character_data['char_id'] in existing_char_ids:
                duplicate_characters.append(character_data['character_name'])
                continue

            new_characters.append(character_data)

        # Write the new characters to characters.json
        existing_characters.extend(new_characters)


        with open('characters.json', 'w') as f:
            json.dump(existing_characters, f)

        # Show a messagebox with the results
        QMessageBox.information(
            self,
            "Import results",
            f"Successfully imported {len(new_characters)} characters.\n"
            f"Found {len(duplicate_characters)} duplicates: {', '.join(duplicate_characters)}.",
        )

        # Refresh the table
        self.table.setRowCount(0) # clear the table
        self.load_data()


class AddCharacterDialog(QDialog):
    def __init__(self, parent=None, initial_data=None):
        super().__init__(parent)
        
        self.setWindowTitle("Add Character")
        
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        
        self.character_name_input = QLineEdit()
        self.account_name_input = QLineEdit()
        self.password_input = QLineEdit()
        self.char_id_input = QLineEdit()
        
        self.layout.addWidget(QLabel("Character Name:"))
        self.layout.addWidget(self.character_name_input)
        self.layout.addWidget(QLabel("Account Name:"))
        self.layout.addWidget(self.account_name_input)
        self.layout.addWidget(QLabel("Password:"))
        self.layout.addWidget(self.password_input)
        self.layout.addWidget(QLabel("Char ID:"))
        self.layout.addWidget(self.char_id_input)
        
        self.submit_button = QPushButton("Submit")
        self.submit_button.clicked.connect(self.accept)
        self.layout.addWidget(self.submit_button)

        if initial_data is not None:
            self.character_name_input.setText(initial_data['character_name'])
            self.account_name_input.setText(initial_data['account_name'])
            self.password_input.setText(initial_data['password'])
            self.char_id_input.setText(initial_data['char_id'])
        
    def get_data(self):
        return {
            'character_name': self.character_name_input.text(),
            'account_name': self.account_name_input.text(),
            'password': self.password_input.text(),
            'char_id': self.char_id_input.text(),
        }

