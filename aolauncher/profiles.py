# profiles.py

import os
import json
import subprocess
from PySide6.QtCore import Qt, QTimer
from PySide6.QtWidgets import QAbstractItemView, QApplication, QCheckBox, QDialog, QFileDialog, QFormLayout, QGridLayout, QHBoxLayout, QHeaderView, QLabel, QLineEdit, QListWidget, QPushButton, QMessageBox, QScrollArea, QSizePolicy, QTableWidget, QTableWidgetItem, QVBoxLayout, QWidget
from PySide6.QtGui import QKeySequence, QShortcut
from utils import create_context_menu


class ProfilesTab(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        # Create a horizontal layout for the buttons
        self.buttonsLayout = QHBoxLayout()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        
        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setSortingEnabled(True)
        self.table.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.table.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
        self.table.setContextMenuPolicy(Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.on_table_context_menu_requested)
        self.table.setHorizontalHeaderLabels(['Profile', 'Characters'])
        
        self.table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        #self.table.setColumnWidth(0, 50)

        for i in range (1, self.table.columnCount()):
            self.table.horizontalHeader().setSectionResizeMode(i, QHeaderView.Stretch)
        self.layout.addWidget(self.table)
      
        self.launchProfileButton = QPushButton("Launch profile(s)")
        self.addProfileButton = QPushButton("Add profile")
        self.editProfileButton = QPushButton("Edit profile")
        self.deleteProfileButton = QPushButton("Delete profile(s)")
        
        # Add buttons to the horizontal layout
        self.buttonsLayout.addWidget(self.launchProfileButton)
        self.buttonsLayout.addWidget(self.addProfileButton)
        self.buttonsLayout.addWidget(self.editProfileButton)
        self.buttonsLayout.addWidget(self.deleteProfileButton)

        # Connect buttons to functions
        self.launchProfileButton.clicked.connect(self.on_launch_profiles_clicked)
        self.addProfileButton.clicked.connect(self.on_add_profile_clicked)
        self.editProfileButton.clicked.connect(self.on_edit_profile_clicked)
        self.deleteProfileButton.clicked.connect(self.on_delete_profile_clicked)

        self.delete_shortcut = QShortcut(QKeySequence(Qt.Key_Delete), self.table)
        self.delete_shortcut.activated.connect(self.on_delete_profile_clicked)

        # Add the buttons layout to the main layout
        self.layout.addLayout(self.buttonsLayout)

        self.load_data()

    def on_table_context_menu_requested(self, point):
        options = [
            ("Launch profile(s)", self.on_launch_profiles_clicked),
            ("Edit profile", self.on_edit_profile_clicked),
            ("Delete profile(s)", self.on_delete_profile_clicked),
        ]
        context_menu = create_context_menu(self, options)
        context_menu.exec_(self.table.mapToGlobal(point))

    def load_data(self):
        try:
            with open('profiles.json', 'r') as f:
                data = json.load(f)
        except FileNotFoundError:
            data = []
            # Create an empty profiles.json
            with open('profiles.json', 'w') as f:
                json.dump(data, f)

        print(f"Loaded data: {data}")

        for profile_data in data:
            print(f"Adding profile: {profile_data}")
            self.add_profile_to_table(profile_data)

    def on_launch_profiles_clicked(self):
        selected_rows = self.table.selectionModel().selectedRows()

        if not selected_rows:
            return

        bat_file_path = os.path.join(os.getcwd(), "Login.bat")

        # Load game path
        with open('settings.json', 'r') as f:
            settings = json.load(f)
        game_path = settings.get('game_path')

        # Fetch character data from table and add to bat file
        account_names = []
        duplicates = []
        with open(bat_file_path, "w") as bat_file:
            bat_file.write("@ECHO OFF\n")
            bat_file.write(f'set "AOPath={game_path}"\n')
            bat_file.write('set "DLLPath=%~dp0AOQuickLauncher.dll"\n')

            # Load existing data from profiles.json
            with open('profiles.json', 'r') as f:
                profiles = json.load(f)

            # Load existing data from characters.json
            with open('characters.json', 'r') as f:
                characters = json.load(f)

            for row_item in selected_rows:
                row = row_item.row()
                profile_name = self.table.item(row, 0).text()

                # Find the data of the profile that needs to be launched
                profile_data = next((profile for profile in profiles if profile['profile_name'] == profile_name), None)
                if profile_data is None:
                    continue

                for character_name in profile_data['characters']:
                    # Find the character data by its name
                    character_data = next((character for character in characters if character['character_name'] == character_name), None)
                    if character_data is None:
                        continue

                    account_name = character_data['account_name']
                    if account_name in account_names:
                        duplicates.append(character_name)
                    else:
                        account_names.append(account_name)
                        password = character_data['password']
                        char_id = character_data['char_id']
                        bat_file.write(f"dotnet %DLLPath% {account_name} {password} {char_id}\n")
                        bat_file.write("timeout /t 1 /nobreak\n")

        if duplicates:
            QMessageBox.warning(
                self,
                "Duplicate Account Names",
                f"One or more characters in the selection are sharing an account with {', '.join(duplicates)}",
            )
            return

        # Run the bat file
        subprocess.run([bat_file_path], shell=True)
        
    def is_valid_profile(self, profile_data, row_being_edited=None):
        # Check if profile name empty
        for i in range(self.table.rowCount()):
            # Skip the row being edited
            if (self.table.item(i, 0).text() == profile_data['profile_name'] and
                i != row_being_edited):
                return False

        return True

        # Load existing data from profiles.json
        with open ('profiles.json', 'r') as f:
            data = json.load(f)

        # Check if the profile name is unique
        for existing_profile_data in data:
            if existing_profile_data['profile_name'] == profile_data['profile_name']:
                return False

        return True

    def show_warning_dialog(self, message):
        QMessageBox.warning(self, "Warning", message)

    def add_profile_to_table(self, profile_data):
        row = self.table.rowCount()  # get current number of rows
        self.table.insertRow(row)  # insert a new row at the end

        # set new row's data
        self.table.setItem(row, 0, QTableWidgetItem(profile_data['profile_name']))  # Profile name
        self.table.setItem(row, 1, QTableWidgetItem(', '.join(profile_data['characters'])))  # Characters in profile

        print(f"Adding profile to table: {profile_data}")

    def add_profile_to_json(self, profile_data):
        # Load existing data from profiles.json
        try:
            with open('profiles.json', 'r') as f:
                data = json.load(f)
        except FileNotFoundError:
            data = []

        # Append new profile data
        data.append(profile_data)

        # Write updated data back to profiles.json
        with open('profiles.json', 'w') as f:
            json.dump(data, f)

    def on_add_profile_clicked(self):
        self.add_profile_dialog = AddProfileDialog(self)
        result = self.add_profile_dialog.exec_()
        if result == QDialog.Accepted:
            # Fetch new profile data from dialog
            new_profile_data = self.add_profile_dialog.get_data()

            # Check if profile data is valid
            while not self.is_valid_profile(new_profile_data):
                print(f"Profile name matches existing profile")
                self.show_warning_dialog("New profile matches existing profile")
                result = self.add_profile_dialog.exec_()
                if result == QDialog.Accepted:
                    new_profile_data = self.add_profile_dialog.get_data()
                else:
                    return

            # Append profile data to table
            self.add_profile_to_table(new_profile_data)
                    
            # Append profile data to table
            self.add_profile_to_json(new_profile_data)

    def on_edit_profile_clicked(self):
        # Get selected profile
        selected_profile = self.table.selectedItems()

        if len(selected_profile) > 2:
            self.show_warning_dialog("Please select only one profile to edit")
            return

        # Get the profile name from the selected item
        profile_name = selected_profile[0].text()

        # Load existing data from profiles.json
        with open('profiles.json', 'r') as f:
            data = json.load(f)

        # Find the data of the profile that needs to be edited
        profile_data = next((profile for profile in data if profile['profile_name'] == profile_name), None)
        if profile_data is None:
            return

        # Open AddProfileDialog with the current profile data
        self.add_profile_dialog = AddProfileDialog(self, profile_data)
        result = self.add_profile_dialog.exec_()
        if result == QDialog.Accepted:
            # Fetch new profile data from dialog
            new_profile_data = self.add_profile_dialog.get_data()

            # Check if profile data is valid
            row_being_edited = self.table.currentRow()
            while not self.is_valid_profile(new_profile_data, row_being_edited):
                self.show_warning_dialog("New profile name matches existing profile")
                result = self.add_profile_dialog.exec_()
                if result == QDialog.Accepted:
                    new_profile_data = self.add_profile_dialog.get_data()
                else:
                    return

            # Update profile data in the table
            self.table.setItem(row_being_edited, 0, QTableWidgetItem(new_profile_data['profile_name']))
            self.table.setItem(row_being_edited, 1, QTableWidgetItem(', '.join(new_profile_data['characters'])))

            # Update profile data in profiles.json
            index = next ((index for (index, d) in enumerate(data) if d["profile_name"] == profile_name), None)
            data[index] = new_profile_data
            with open('profiles.json', 'w') as f:
                json.dump(data, f)

    def on_delete_profile_clicked(self):
        # Get selected profile(s)
        selected_profiles = self.table.selectedItems()
        
        # Check if any profiles are selected
        if not selected_profiles:
            return

        # Find unique rows (profiles) from selected items
        selected_rows = list(set(item.row() for item in selected_profiles))

        # More than one profile selected, ask for confirmation
        if len(selected_rows) > 1:
            profile_names = [self.table.item(row, 0).text() for row in selected_rows]
            message = "Are you sure you want to delete the following profiles?\n" + '\n'.join(profile_names)
            reply = QMessageBox.question(self, 'Confirm deletion', message,
                        QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                        
            if reply == QMessageBox.No:
                return

        # Load existing data from profiles.json
        with open('profiles.json', 'r') as f:
            data = json.load(f)

        for row in selected_rows:
            # Get the profile name from the row
            profile_name = self.table.item(row, 0).text()

            # Find the data of the profile that needs to be deleted
            index = next((index for (index, d) in enumerate(data) if d["profile_name"] == profile_name), None)

            # Remove the profile from the data
            if index is not None:
                data.pop(index)

        # Write updated data back to the profile.json
        with open ('profiles.json', 'w') as f:
            json.dump(data, f)

        # Delete rows from the table (from end to start, to avoid changing the indices of rows to be deleted)
        for row in sorted(selected_rows, reverse=True):
            self.table.removeRow(row)

    def refresh_profiles(self):
        self.table.clearContents()
        self.table.setRowCount(0)
        self.load_data()
            
class CharacterCheckBox(QCheckBox):
    def __init__(self, character, parent=None):
        super().__init__(f"{character['character_name']} ({character['account_name']})", parent)
        self.character = character
        print(f"Created checkbox for character: {character}")


class AddProfileDialog(QDialog):
    def __init__(self, parent=None, initial_data=None):
        super().__init__(parent)
        self.setWindowTitle("Add/Edit Profile")

        # Create a layout
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        
        # Profile name input
        self.profile_name_input = QLineEdit()
        self.layout.addWidget(QLabel("Profile name:"))
        self.layout.addWidget(self.profile_name_input)
        
        # Grid layout for character checkboxes and selected characters list
        self.grid_layout = QGridLayout()
        self.layout.addLayout(self.grid_layout)
        
        # Scroll Area for character checkboxes
        self.scroll_widget = QWidget()
        self.scroll_layout = QFormLayout(self.scroll_widget)
        self.scroll_area = QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.scroll_widget)
        
        # Characters list
        self.characters_list = QListWidget()
        self.characters_list.setDragDropMode(QListWidget.InternalMove)
        self.characters_list.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)

        # Load characters from JSON file
        with open('characters.json', 'r') as f:
            characters = json.load(f)

        self.character_checkboxes = {}
        for i, character in enumerate(characters):
            checkbox = CharacterCheckBox(character)
            checkbox.stateChanged.connect(self.update_character_list)
            print(f"Connected signal for checkbox: {checkbox.text()}")
            self.character_checkboxes[character['character_name']] = checkbox
            self.scroll_layout.addRow(checkbox)

        self.grid_layout.addWidget(self.scroll_area, 0, 0)
        self.grid_layout.addWidget(self.characters_list, 0, 1)

        # Move up and move down buttons
        self.move_up_button = QPushButton("Move Up")
        self.move_down_button = QPushButton("Move Down")
        self.move_up_button.clicked.connect(self.move_up)
        self.move_down_button.clicked.connect(self.move_down)
        
        self.grid_layout.addWidget(self.move_up_button, 1, 1)
        self.grid_layout.addWidget(self.move_down_button, 2, 1)

        self.submit_button = QPushButton("Submit")
        self.submit_button.clicked.connect(self.accept)
        self.layout.addWidget(self.submit_button)

        if initial_data is not None:
            self.profile_name_input.setText(initial_data['profile_name'])
            # Check the checkboxes of characters in the profile
            for character in initial_data['characters']:
                if character in self.character_checkboxes:
                    self.character_checkboxes[character].setCheckState(Qt.Checked)
                    self.character_checkboxes[character].stateChanged.emit(Qt.Checked)

            # Load the characters in the order they are in profiles.json
            for character in initial_data['characters']:
                self.characters_list.addItem(character)

    def move_up(self):
        current_row = self.characters_list.currentRow()
        if current_row >= 1:
            current_item = self.characters_list.takeItem(current_row)
            self.characters_list.insertItem(current_row - 1, current_item)
            self.characters_list.setCurrentRow(current_row - 1)

    def move_down(self):
        current_row = self.characters_list.currentRow()
        if current_row < self.characters_list.count() - 1:
            current_item = self.characters_list.takeItem(current_row)
            self.characters_list.insertItem(current_row + 1, current_item)
            self.characters_list.setCurrentRow(current_row + 1)

    def update_character_list(self, state):
        checkbox = self.sender()
        character = checkbox.character
        print(f"update_character_list called, state = {state}, character = {character}")

        if state == 2:
            # Check if there is already a checkbox with the same account_name checked
            for other_checkbox in self.character_checkboxes.values():
                if other_checkbox != checkbox and other_checkbox.isChecked() and other_checkbox.character['account_name'] == character['account_name']:
                    # Disconnect stateChanged signal, uncheck the checkbox and then reconnect the signal
                    checkbox.stateChanged.disconnect()
                    checkbox.setChecked(False)
                    QApplication.processEvents()  # Force the application to process events immediately.
                    checkbox.stateChanged.connect(self.update_character_list)
                    self.show_warning_dialog("Cannot add two characters with the same account name to one profile")
                    return

            self.characters_list.addItem(character['character_name'])
            print(f"Adding character: {character['character_name']}")
        elif state == 0:
            for i in range(self.characters_list.count()):
                if self.characters_list.item(i).text() == character['character_name']:
                    self.characters_list.takeItem(i)
                    break
        print('Current characters: ', [self.characters_list.item(i).text() for i in range(self.characters_list.count())])

        self.characters_list.repaint()

    def show_warning_dialog(self, message):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText(message)
        msg.setWindowTitle("Warning")
        msg.exec_()

    def get_data(self):
        # Collect character names from the list
        characters = [self.characters_list.item(i).text() for i in range(self.characters_list.count())]

        # Return new profile data
        return {
            "profile_name": self.profile_name_input.text(),
            "characters": characters
        }
