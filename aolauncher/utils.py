# utils.py

import requests
import time
import json
from PySide6.QtWidgets import QMenu

# The map of professions to their abbreviations
PROFESSIONS_ABBREVIATIONS = {
    "Adventurer": "ADV",
    "Agent": "AGENT",
    "Bureaucrat": "CRAT",
    "Doctor": "DOC",
    "Enforcer": "ENF",
    "Engineer": "ENG",
    "Fixer": "FIX",
    "Keeper": "KEEP",
    "Martial Artist": "MA",
    "Meta-Physicist": "MP",
    "Nano-Technician": "NT",
    "Soldier": "SOL",
    "Shade": "SHA",
    "Trader": "TRA",
}

def create_context_menu(parent, options):
    context_menu = QMenu(parent)
    for option_text, option_slot in options:
        action = context_menu.addAction(option_text)
        action.triggered.connect(option_slot)
    return context_menu

def get_character_bio(character_name):
    url = f"https://people.anarchy-online.com/character/bio/d/5/name/{character_name}/bio.xml?data_type=json"
    response = requests.get(url)
    data = response.json()
    if data:  # check if data is not None or not an empty list
        levelx = data[0]['LEVELX']
        alienlevel = data[0]['ALIENLEVEL']
        prof = data[0]['PROF']
        prof_abbr = PROFESSIONS_ABBREVIATIONS.get(prof, prof)  # Get the abbreviation, or use the profession if there is no abbreviation
        bio = f"{levelx}/{alienlevel} {prof_abbr}"
        return bio
    else:
        print(f"No bio data for character {character_name}")
        return ""


def save_character_data(character_name):
    # Load the existing data
    with open('characters.json', 'r') as f:
        data = json.load(f)

    # Find the character in the list and update it
    for i, char in enumerate(data):
        if char['character'] == character_name['character']:
            data[i] = character_name
            break

    # Save the data back to the file
    with open('characters.json', 'w') as f:
        json.dump(data, f)