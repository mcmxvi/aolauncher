# about.py

from PySide6.QtCore import Qt
from PySide6.QtGui import QTextCursor
from PySide6.QtWidgets import QTextEdit, QVBoxLayout, QWidget

class AboutTab(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()

        self.text_edit = QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit.setTextInteractionFlags(Qt.TextSelectableByMouse)

        layout.addWidget(self.text_edit)
        self.setLayout(layout)

        self.insert_text("AO Launcher\n\n")
        self.insert_text("Version 0.1\n\n")
        self.insert_text("GUI developed by: Strupstad (RK1/RK5)\n")
        self.insert_text("Backend developed by NeverKnowsBest, and available here\n")
        self.insert_text("https://gitlab.com/never-knows-best/aoquicklauncher/")
        self.insert_text("\n\nThis software is open source and available at\n")
        self.insert_text("https://gitlab.com/mcmxvi/aolauncher")
        self.insert_text("\n\nPlease visit the repository for updates and to report issues.")

    def insert_text(self, text):
        self.text_edit.append(text)  # QTextEdit.append adds a new paragraph.
