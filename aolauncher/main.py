# main.py
# Requirements from 'pip install' are: PySide6, requests, openpyxl

from PySide6.QtWidgets import QApplication, QMainWindow, QTabWidget, QWidget
from PySide6.QtCore import Qt, QSettings
import sys
from characters import CharactersTab
from profiles import ProfilesTab
from settings import SettingsTab
from help import HelpTab
from about import AboutTab


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("AO Launcher")
        self.resize(800, 600)

        self.settings = QSettings('MyCompany', 'MyApp')
        self.restoreGeometry(self.settings.value('windowGeometry'))
        self.restoreState(self.settings.value('windowState'))

        tab_widget = QTabWidget()
        self.setCentralWidget(tab_widget)

        # Create ProfilesTab before CharactersTab
        profiles_tab = self.createProfilesTab()

        # Create tabs and pass profiles_tab to CharactersTab
        tab_widget.addTab(self.createCharactersTab(profiles_tab), "Characters")
        tab_widget.addTab(profiles_tab, "Profiles")
        tab_widget.addTab(self.createSettingsTab(), "Settings")
        tab_widget.addTab(self.createHelpTab(), "Help")
        tab_widget.addTab(self.createAboutTab(), "About")

    def createCharactersTab(self, profiles_tab):
        # For now, just return a placeholder QWidget
        return CharactersTab(profiles_tab)

    def createProfilesTab(self):
        # Placeholder QWidget
        return ProfilesTab()

    def createSettingsTab(self):
        # Placeholder QWidget
        return SettingsTab()

    def createHelpTab(self):
        # Placeholder QWidget
        return HelpTab()

    def createAboutTab(self):
        # Placeholder QWidget
        return AboutTab()

    def closeEvent(self, event):
        # Save window state and geometry on close
        self.settings.setValue('windowGeometry', self.saveGeometry())
        self.settings.setValue('windowState', self.saveState())
        super().closeEvent(event)

if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec())
