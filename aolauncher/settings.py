# settings.py

import os
import json
import tarfile
import datetime
from pathlib import Path
from PySide6.QtWidgets import QFileDialog, QLabel, QPushButton, QHBoxLayout, QVBoxLayout, QMessageBox, QWidget, QFrame, QMessageBox
from PySide6.QtCore import QSettings

class SettingsTab(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        # Create layout
        layout = QVBoxLayout()

        # Create game path frame and layout
        game_path_frame = QFrame(self)
        game_path_layout = QVBoxLayout(game_path_frame)
        # Create game path widgets
        self.path_label = QLabel("Please select the folder where 'Anarchy Online' is installed:", self)
        self.display_path_label = QLabel("", self)
        self.browse_button = QPushButton("Browse", self)
        # Connect button clicked signal to slot
        self.browse_button.clicked.connect(self.browse)
        # Add widgets to game path layout
        game_path_layout.addWidget(self.path_label)
        game_path_layout.addWidget(self.display_path_label)
        game_path_layout.addWidget(self.browse_button)

        # Create backup frame and layout
        backup_frame = QFrame(self)
        backup_layout = QVBoxLayout(backup_frame)
        # Create backup and restore buttons
        self.backup_info_label = QLabel("Backup saves the characters.json, profiles.json and settings.json to a zip-file", self)
        self.backup_button = QPushButton("Backup", self)
        self.restore_button = QPushButton("Restore", self)
        # Connect buttons' clicked signals to slots
        self.backup_button.clicked.connect(self.backup)
        self.restore_button.clicked.connect(self.restore)
        # Add widgets to backup layout
        backup_layout.addWidget(self.backup_info_label)
        backup_layout.addWidget(self.backup_button)
        backup_layout.addWidget(self.restore_button)

        # Add frames to main layout
        layout.addWidget(game_path_frame)
        layout.addStretch(1)
        layout.addWidget(backup_frame)

        self.setLayout(layout)

        # Load and display the saved path
        self.load_settings()
        
        if not self.display_path_label.text():
            QMessageBox.warning(self, 'Warning', 'Game path is not set. Please go to the Settings tab and select the game path.', QMessageBox.Ok)

    def browse(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.Directory)
        if file_dialog.exec_():
            selected_dir = file_dialog.selectedFiles()[0]
            selected_dir = str(Path(selected_dir))
            self.display_path_label.setText(selected_dir)
            self.save_settings()

    def load_settings(self):
        # Ensure settings.json exists
        if not os.path.isfile("settings.json"):
            with open('settings.json', 'w') as f:
                json.dump({}, f)

        # Load settings
        with open('settings.json', 'r') as f:
            data = json.load(f)
        if 'game_path' in data:
            self.display_path_label.setText(data['game_path'])

    def save_settings(self):
        # Load existing settings
        with open('settings.json', 'r') as f:
            settings = json.load(f)
        # Update settings
        settings['game_path'] = self.display_path_label.text()
        # Save settings
        with open('settings.json', 'w') as f:
            json.dump(settings, f)

    def backup(self):
        # Create a tar file
        backup_file = f'aolauncher_backup_{datetime.datetime.now().strftime("%Y%m%d%H%M%S")}.tar.gz'
        with tarfile.open(backup_file, 'w:gz') as f:
            f.add('characters.json')
            f.add('profiles.json')
            f.add('settings.json')

        QMessageBox.information(self, 'Backup Complete', f'Backup saved as {backup_file}.', QMessageBox.Ok)

    def restore(self):
        # Open file dialog to select a backup file
        file_dialog = QFileDialog()
        backup_file, _ = file_dialog.getOpenFileName(self, "Select Backup File", "", "Tar Files (*.tar.gz)")
        if backup_file:  # if a file was selected
            try:
                with tarfile.open(backup_file, 'r:gz') as f:
                    f.extractall()  # Extract all files
                QMessageBox.information(self, 'Restore Complete', 'Restore completed successfully. Please restart the program', QMessageBox.Ok)
            except Exception as e:
                QMessageBox.critical(self, 'Error', f'An error occurred while restoring: {str(e)}', QMessageBox.Ok)
