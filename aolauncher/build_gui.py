import os
import shutil
import subprocess
import logging

# Set up logging
logging.basicConfig(filename='build.log', level=logging.INFO, 
                    format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

try:
    # The path to your python script
    python_script_path = "main.py"

    # The directories where the built executable should be moved
    target_directories = [os.path.join("..", "Build", build_type, "net5.0") for build_type in ["Release", "Debug"]]

    # Change working directory to script directory
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    # Delete previous build folders and executables if they exist
    if os.path.exists('dist'):
        shutil.rmtree('dist')
    if os.path.exists('build'):
        shutil.rmtree('build')

    # Build the executable
    logging.info("Building the executable...")
    result = subprocess.run(['pyinstaller', '--onefile', '--name', 'AO Launcher', python_script_path], capture_output=True)
    logging.info(result.stdout.decode())
    logging.error(result.stderr.decode())

    # Move the executable to the target directories
    for target_directory in target_directories:
        # Ensure the target directory exists
        os.makedirs(target_directory, exist_ok=True)

        # Delete the existing executable if it exists
        if os.path.exists(os.path.join(target_directory, 'AO Launcher.exe')):
            os.remove(os.path.join(target_directory, 'AO Launcher.exe'))

        # Absolute paths for logging
        source_path = os.path.abspath(os.path.join('dist', 'AO Launcher.exe'))
        target_path = os.path.abspath(os.path.join(target_directory, 'AO Launcher.exe'))

        logging.info(f'Moving the executable from {source_path} to {target_path}...')
        shutil.copy(source_path, target_path)
        logging.info("Executable copied successfully!")

except Exception as e:
    logging.error(f'An error occurred: {e}')
